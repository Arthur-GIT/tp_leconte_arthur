"Question 1"
"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

a = 1
dt = 0.001
t = np.arange(0.0, 2.0, dt)
f1 = 5
f2 = 30

sin5 = np.sin(2*np.pi*f1*t) 
sin30 = + np.sin(2*np.pi*f2*t)
avfiltre = sin5 + sin30

sos = signal.cheby1(10 ,1, 25, 'hp', fs=2000, output='sos')
filtered = signal.sosfilt(sos, avfiltre)

fig, ax = plt.subplots(3)

ax[0].plot(sin30)
ax[0].set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax[0].grid()

ax[1].plot(avfiltre)
ax[1].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[1].grid()

ax[2].plot(filtered)
ax[2].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[2].grid()

plt.show()
"""
'Question 2'
"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

a = 1
dt = 0.001
t = np.arange(0.0, 1.0, dt)
f1 = 5
f2 = 30

sin5 = np.sin(2*np.pi*f1*t) 
sin30 = + np.sin(2*np.pi*f2*t)
avfiltre = sin5 + sin30

#sos = signal.cheby2(12 ,20, 25, 'hp', fs=2000, output='sos')
#sos = signal.butter(10, 15, 'hp', fs=1000, output='sos')
filtered = signal.sosfilt(sos, avfiltre)

fig, ax = plt.subplots(3)

ax[0].plot(sin30)
ax[0].set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax[0].grid()

ax[1].plot(avfiltre)
ax[1].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[1].grid()

ax[2].plot(filtered)
ax[2].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[2].grid()

plt.show()
"""
'Question 4'
"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

a = 1
dt = 0.001
t = np.arange(0.0, 1.0, dt)
f1 = 5
f2 = 30

sin5 = np.sin(2*np.pi*f1*t) 
sin30 = + np.sin(2*np.pi*f2*t)
avfiltre = sin5 + sin30

sos = signal.ellip(8 ,1,100, 25 ,'hp', fs=2000, output='sos')
filtered = signal.sosfilt(sos, avfiltre)
fig, ax = plt.subplots(3)

ax[0].plot(sin30)
ax[0].set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax[0].grid()

ax[1].plot(avfiltre)
ax[1].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[1].grid()

ax[2].plot(filtered)
ax[2].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[2].grid()

plt.show()
"""
'Question 7 V1'
"""
import numpy as np
import matplotlib.pyplot as plt

def signal(t): return (np.sin(np.pi * t))/(np.pi*t)
# Echantillonnage
D = 10. # Duree d'observation
fe = 7. # Frequence d'echantillonnage
N = int(D * fe) + 1 # Nombre de points enregistres
te = np.linspace(-10., (N-1)/fe, N) # Grille d'echantillonnage
tp = np.linspace(-10., D, 1000) # Grille plus fine pour tracer l'allure du signal parfait
# Trace du signal
plt.plot(te, signal(te), 'or-', label = u"Signal echantillonne")
plt.plot(tp, signal(tp), 'b--', label = u"Signal reel")
plt.grid()
plt.xlabel("Temps $t$")
plt.ylabel("Amplitude $x(t)$")
plt.legend()
plt.show() """

'Question 7 V2'

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

f0 = 5
fe = 5
dt = 0.001
dt1 = 1/fe
t = np.arange(-2.0, 2.0, dt)
t1 = np.arange(-2.0, 2.0, dt1)

signal = (np.sin(np.pi*f0*t))/(np.pi*t)
signal_echant = (np.sin(np.pi*t1*f0))/(np.pi*t1)

fig,(ax1) = plt.subplots()

ax1.plot(t, signal)
ax1.plot(t1, signal_echant, '-o')
ax1.set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax1.grid()
plt.show()

'Question 8'

"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy import integrate

f0 = 5
fe = 10
dt = 0.001
dt1 = 0.1
te = 1/fe
n = 1
t = np.arange(-2.0, 2.0, dt)

def sig(t):
    return (np.sin(np.pi*f0*t))/(np.pi*t)

echant = int((1/dt1))* (integrate.quad(sig, (n*te), (n*te)+(dt1)))

print(echant)

fig,(ax1) = plt.subplots()

ax1.plot(t, sig)
ax1.plot(te, echant)
ax1.set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax1.grid()
plt.show()
"""
