#Question 1

#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#from cv2 import cv2

#mpimg.imsave(r"C:\Users\arthu\OneDrive\Documents\Cours\INSA\3eme année\Traitement du Signal et Image\TP5\image2.png", img ,format='png')
#print(img)
#imgplot = plt.imshow(img, cmap='gray')

#x = [1,2,2,3,4,4,4,4,4,5,5]

#plt.hist(img.ravel(),256,[0,256])

#plt.xlabel('valeurs')
#plt.ylabel('nombres')
#plt.title('Exemple d\'histogramme simple')

#plt.show()


#Question 1 bis#

from cv2 import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread(r'C:\Users\arthu\Desktop\paysage.jpg')
img = cv2.resize(img,(int(600), int(600)))
#img = img[50:200, 50:200]
#img = cv2.GaussianBlur(img, (3, 3), 3)
#img = cv2.medianBlur(img, 3)
#img = cv2.flip(img)

cv2.imshow('image',img)
#cv2.imwrite("save_photo.pdf", img) 
cv2.waitKey(0)
cv2.destroyAllWindows()

hist = cv2.calcHist([img], [0], None, [256], [0, 256])
plt.plot(hist)
plt.show()