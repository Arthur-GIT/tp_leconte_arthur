"""
#TP1 QUESTION 1 A 6

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

#Datas for plotting

t = np.arange(-5, 5, 0.01)
#s = 1 + np.sin(2*np.pi*t) #SINUS
#s = np.random.random_sample(1000) #QUESTION 1 : SIGNAL NON STATIONNAIRE
#s = (1 * abs(t)<0.5) #QUESTION 2 : SIGNAL PORTE
#s = np.exp(-t) #QUESTION 3 : EXPONENTIELLE INVERSEE
#s = 1 + np.sin(2*np.pi*t) + np.random.random_sample(1000) #QUESTION 4 : SIGNAL ET BRUIT

#QUESTION 5 : SIN, COS, SIN+COS
#s = np.sin(t)
#u = np.cos(t)
#v = s + u


s = np.exp(1j*100*np.pi*t) #QUESTION 6 

print(s)

fig, ax = plt.subplots()
ax.plot(t, s)

#fig, ax = plt.subplots(3)
#ax[0].plot(t, s)
#ax[1].plot(t, u)
#ax[2].plot(t, v)

ax.set(xlabel='time (s)', ylabel='voltage (mV)', title='expression')
ax.grid()

#ax[0].set(xlabel='time (s)', ylabel='voltage (mV)', title='sin')
#ax[0].grid()
#ax[1].set(xlabel='time (s)', ylabel='voltage (mV)', title='cos')
#ax[1].grid()
#ax[2].set(xlabel='time (s)', ylabel='voltage (mV)', title='sin + cos')
#ax[2].grid()

plt.show()



"""
#TP1 QUESTION 7 ET SUITE

import matplotlib
import numpy as np
import matplotlib.pyplot as plt

# definition du signal
dt = 0.01
T1 = -5
T2 = 5
t = np.arange(T1, T2, dt)
signal = np.exp(-1*np.abs(t)) #QUESTION 7

# affichage du signal
plt.subplot(211)
#plt.plot(t,signal)

# calcul de la transformee de Fourier et des fréquences
fourier = np.fft.fft(signal) #QUESTION 8
inverse = abs(np.fft.ifft(signal)) #QUESTION 11
n = signal.size
freq = np.fft.fftfreq(n, d=dt) 
approx = np.fft.fftshift(signal) #QUESTION 10

plt.plot(t, signal)

# affichage de la transformee de Fourier
"""
plt.subplot(212)
plt.plot(freq, fourier.real, label="real")
plt.plot(freq, fourier.imag, label="imag")
"""

# QUESTION 9 : affichage de la phase de la transformee de Fourier


module = np.abs(fourier)
phase = np.angle(fourier)
plt.subplot(212)
#plt.plot(t, module, label="module")
#plt.plot(t, phase, label="phase")

#QUESTION 12 : affichage de la transformee inverse


plt.subplot(212)
plt.plot(freq, signal.real, label="real")
plt.plot(freq, signal.imag, label="imag")


#QUESTION 10 : affichage de l'approximation de la transformée"

"""
plt.subplot(212)
plt.plot(freq, inverse, label="Inverse")
"""

plt.legend()
plt.show()
