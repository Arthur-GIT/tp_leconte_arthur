"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

a = 1
dt = 0.01
t = np.arange(-1.0, 5.0, dt)

t1 = 2
t2 = 4
f=[]
print (len(t))
for i in range(len(t)):
    if t[i] >= t1 and t[i] <= t2 :
        f.append(1)
    else :
        f.append(0)
        
g = np.exp(-a*t)

correl = signal.convolve(f, g)
#correl = signal.correlate(f, g)

fig, ax = plt.subplots(3)

ax[0].plot(correl)
ax[0].set(xlabel="time (s)", ylabel="voltage (mV)", title="TP2")
ax[0].grid()

ax[1].plot(f)
ax[1].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[1].grid()

ax[2].plot(g)
ax[2].set(xlabel="time (s)", ylabel="voltage (mV)", title="")
ax[2].grid()

plt.show()
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

a = 1
dt = 0.01
t = np.arange(-1.0, 5.0, dt)

t1 = 2
t2 = 4
f=[]
print (len(t))
for i in range(len(t)):
    if t[i] >= t1 and t[i] <= t2 :
        f.append(1)
    else :
        f.append(0)
        
g = np.exp(-a*t)


g1 = f[:3]
g2 = g[:5]
i = signal.lti(g1, g2)
j1, z1 = signal.step(i)
j2, z2 = signal.impulse(i)
plt.plot(j1,z1)
plt.plot(j2,z2)

plt.show()
